package com.example.bignerdranch.androidtutorials;

import com.example.bignerdranch.androidtutorials.common.Constants;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;

public class CrimeActivity extends FragmentActivity {

	@TargetApi(11)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_crime);
		
		FragmentManager fm = getSupportFragmentManager();
	
		Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
		if (fragment == null) {
			fragment = new CrimeFragment();
			fm.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
		}
		
		if (savedInstanceState != null) {
        	
        }
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        	ActionBar actionBar = getActionBar();
        	actionBar.setSubtitle(R.string.app_subtitle);
        }
        
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_ONCREATE);
        }
        
	}
	
    
    @Override
    public void onStart() {
    	super.onStart();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_ONSTART);
		}
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_ONSTOP);
		}
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_ONRESUME);
		}
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_ONPAUSE);
		}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_ONDESTROY);
		}
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_SAVESTATE);
		}
    	
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	switch (requestCode) {
    		
    		case Constants.REQUESTCODE_OTHERACTIVITY:
    			if (data != null) {
    				
    				Log.d(Constants.TAG_CRIMEACTIVITY, Constants.MSG_OTHERACTIVITY_RESULT);
    			}
    			break;
    	}
    }

}
