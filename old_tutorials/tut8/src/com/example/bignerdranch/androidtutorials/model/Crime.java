package com.example.bignerdranch.androidtutorials.model;

import java.util.Date;
import java.util.UUID;

public class Crime {
	
	public Date getDate() {
		return this.mDate;
	}

	public void setDate(Date mDate) {
		this.mDate = mDate;
	}

	public boolean isSolved() {
		return this.mSolved;
	}

	public void setSolved(boolean mSolved) {
		this.mSolved = mSolved;
	}

	private String mTitle;
	private UUID mId;
	private Date mDate;
	private boolean mSolved;
	
	public Crime() {
		// create unique id
		mId = UUID.randomUUID();
		mDate = new Date();
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	public UUID getID() {
		return mId;
	}

}
