package com.example.bignerdranch.androidtutorials;

public class Constants {
	protected static final Boolean DEBUG = true;
	
	public static final int REQUESTCODE_CHEATACTIVITY = 0;
	
	public static final String EXTRAKEY_ISANSWERTRUE = "EXTRA_key_is_this_answer_true";
	public static final String EXTRAKEY_ANSWERSHOWN = "EXTRA_answer_shown_to_user";
	
	public static final String SAVEDINSTANCEKEY_QUESTIONINDEX = "SAVEDINSTANCE_key_question_index";
	public static final String SAVEDINSTANCEKEY_QUESTIONCHEATEDINDEX = "SAVEDINSTANCE_key_question_index_when_user_cheated";
	public static final String SAVEDINSTANCEKEY_ANSWERSHOWN = "SAVEDINSTANCE_answer_shown_to_user";
	public static final String SAVEDINSTANCEKEY_ISCHEATER = "SAVEDINSTANCE_user_did_cheat";

	public static final String TAG_QUIZACTIVITY = "QuizActivity";
	public static final String TAG_CHEATACTIVITY = "CheatActivity";

	public static final String MSG_PREVIOUSQUESTION = "Previous Question called.";
	public static final String MSG_CREATEQUESTIONS = "Question Bank generated.";
	public static final String MSG_NEXTQUESTION = "Next Question called.";
	public static final String MSG_ONCREATE = "onCreate called.";
	public static final String MSG_ONRESUME = "onResume called.";
	public static final String MSG_ONPAUSE = "onPause called.";
	public static final String MSG_ONSTOP = "onStop called.";
	public static final String MSG_ONDESTROY = "onDestroy called.";
	public static final String MSG_ONSTART = "onStart called.";
	public static final String MSG_SAVESTATE = "onSaveInstanceState called.";
	public static final String MSG_CHECKANSWER = "checkAnswer called.";
	public static final String MSG_USERCHEATED = "User Cheated variable is set.";
}
