package com.example.bignerdranch.androidtutorials;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.util.AndroidException;
import android.util.Log;

// ***
public class QuizActivity extends Activity {

    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mPreviousButton;
    private TextView mQuestionTextView;
    public final List<TrueFalse> mQuestionBank = generateQuestionBank();
    private int[] mQuestionIndices = {0,0,0,0};
    public final int mNumberOfQuestions = this.mQuestionBank.size();
    public final Random _mRandomizer = new Random(System.currentTimeMillis());
    
    private ArrayList<TrueFalse> generateQuestionBank() {
        ArrayList<TrueFalse> tmp = new ArrayList<TrueFalse>();
        tmp.add(new TrueFalse(R.string.question_africa, false));
        tmp.add(new TrueFalse(R.string.question_mideast, false));
        tmp.add(new TrueFalse(R.string.question_nz, false));
        tmp.add(new TrueFalse(R.string.question_americas, true));
        tmp.add(new TrueFalse(R.string.question_asia, true));
        tmp.add(new TrueFalse(R.string.question_oceans, true));
        
        if (Constants.DEBUG) {
        	Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_CREATEQUESTIONS, new AndroidException());
        }
        return tmp;
    }

    private void getNextQuestion() {
    	int tmp;
    	this.mQuestionIndices[0] = this.mQuestionIndices[1];
    	this.mQuestionIndices[1] = this.mQuestionIndices[2];
    	this.mQuestionIndices[2] = this.mQuestionIndices[3];
    	tmp = this.mQuestionIndices[3];
    	while (tmp == this.mQuestionIndices[2] || tmp == this.mQuestionIndices[1] || tmp == this.mQuestionIndices[0]) {
    		tmp = this._mRandomizer.nextInt(this.mNumberOfQuestions);
    	}
    	this.mQuestionIndices[3] = tmp;
        int question = this.mQuestionBank.get(this.mQuestionIndices[3]).getQuestion();
        this.mQuestionTextView.setText(question);
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_NEXTQUESTION);
		}
    }
    
    private void getPreviousQuestion() {
    	this.mQuestionIndices[3] = this.mQuestionIndices[2];
    	this.mQuestionIndices[2] = this.mQuestionIndices[1];
    	this.mQuestionIndices[1] = this.mQuestionIndices[0];
        int question = this.mQuestionBank.get(this.mQuestionIndices[3]).getQuestion();
        this.mQuestionTextView.setText(question);
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_PREVIOUSQUESTION);
		}
    }
    
    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = this.mQuestionBank.get(this.mQuestionIndices[3]).isTrueQuestion();
        
        int messageResId = 0;
        
        if (userPressedTrue == answerIsTrue) {
            messageResId = R.string.correct_toast;
        } else {
            messageResId = R.string.incorrect_toast;
        }
        
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_CHECKANSWER,
					new AndroidException());
		}
        
		Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        if (savedInstanceState != null) {
        	this.mQuestionIndices = savedInstanceState.getIntArray(Constants.SAVEDINSTANCEKEY_QUESTIONINDEX);
        }
        
        this.mQuestionTextView = (TextView)findViewById(R.id.question_text_view);

        this.mTrueButton = (Button) findViewById(R.id.true_button);
        this.mTrueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuizActivity.this.checkAnswer(true);
            }
        });
        
        this.mFalseButton = (Button) findViewById(R.id.false_button);
        this.mFalseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuizActivity.this.checkAnswer(false);
            }
        });
        
        this.mNextButton = (ImageButton) findViewById(R.id.next_button);
        this.mNextButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                QuizActivity.this.getNextQuestion();
            }
        });
        
        this.mPreviousButton = (ImageButton) findViewById(R.id.previous_button);
        this.mPreviousButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                QuizActivity.this.getPreviousQuestion();
            }
        });
        
        this.getNextQuestion();   
        
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONCREATE);
		}
    }
    
    public void onStart() {
    	super.onStart();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONSTART);
		}
    }
    
    public void onStop() {
    	super.onStop();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONSTOP);
		}
    }
    
    public void onResume() {
    	super.onResume();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONRESUME);
		}
    }
    
    public void onPause() {
    	super.onPause();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONPAUSE);
		}
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONDESTROY);
		}
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_SAVESTATE);
		}
		outState.putIntArray(Constants.SAVEDINSTANCEKEY_QUESTIONINDEX, mQuestionIndices);
    }
}
