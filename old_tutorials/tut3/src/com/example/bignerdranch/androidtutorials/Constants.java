package com.example.bignerdranch.androidtutorials;

public class Constants {
	protected static final Boolean DEBUG = true;
	
	public static final String TAG_QUIZACTIVITY = "QuizActivity";
	
	public static final String SAVEDINSTANCEKEY_QUESTIONINDEX = "key_question_index";

	public static final String MSG_PREVIOUSQUESTION = "Previous Question called.";
	public static final String MSG_CREATEQUESTIONS = "Question Bank generated.";
	public static final String MSG_NEXTQUESTION = "Next Question called.";
	public static final String MSG_ONCREATE = "onCreate called.";
	public static final String MSG_ONRESUME = "onResume called.";
	public static final String MSG_ONPAUSE = "onPause called.";
	public static final String MSG_ONSTOP = "onStop called.";
	public static final String MSG_ONDESTROY = "onDestroy called.";
	public static final String MSG_ONSTART = "onStart called.";
	public static final String MSG_SAVESTATE = "onSaveInstanceState called.";
	public static final String MSG_CHECKANSWER = "checkAnswer called.";
}
