Tutorial 3

Improving GeoQuiz, Logging, Activity Lifecycles.

Fix a fault causing the question index to change when the device is rotated.
Create layouts for different orientations.
Use logging to identify activity behaviour.


Logging:

android.util.Log package.

Log.loggingMethod(tagString, valueString)

the logging method defines the priority level of the log message.
tagStrings allow log messages to be sorted and searched in the LogCat view.
tagStrings and ValueStrings will appear in LogCat. they can be tailored for detailed analysis.


Layouts:

There can be portrait and landscape layouts.

portrait: layout

landscape: layout-land

reorientation changes the layout. changed layouts cause the app to recreate.



drawable-xxxxx:	icons and pictures, for each screen density.

ldpi:	deprecated.
mdpi:	160dpi
hdpi:	240dpi
xdpi:	320dpi
xxdpi:	>320dpi



Activity lifecycle:

non-existent		stopped, invisible		paused, partially visible		running, visible, foreground
	->		onCreate		->		onStart			->				onResume		->
	<-		onDestroy		<-		onStop			<-				onPause			<-
	

When paused, stopped or destroyed, an instance state is saved to memory and remains until flushed, usually
on device reboot or garbage collection after a long time. also pressing the back button to exit an app will
flush the instance state.

onCreate(Bundle...):		The Bundle parameter is the instance state.

Instance states record data using keyStrings and values. The type of value depends on the type of storage method,
effectively a setter or putter method, or rather the type of setter method matches the type of the value.

onSavedInstanceState(Bundle...)
{ ...

	BundleInstance.putType(String keyString, Type value)

}


Recreation was causing the GeoQuiz question to change. Saving data to the bundle and extracting it later prevented
this fault.


Controller Activity structure:

The Controller activity class (or master controller), should define overrides for the onCreate, onDestroy, onStart,
onStop, onResume, and onPause methods. Each of these should invoke the super method then perform it's own logic.

In addition the onSaveInstanceState method should be overridden and important data saved in the bundle. The above
methods should extract the required data where necessary for continuing the current activity.


Method							Type of data to extract

onCreate						Hardware setup, persistent or permanent values

onStart							data affecting layout appearance?

onResume						frequently updated data