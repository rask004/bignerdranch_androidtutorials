package com.androidTutorial;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuizActivity extends Activity {

    private Button mTrueButton;
    private Button mFalseButton;
    private Button mNextButton;
    private TextView mQuestionTextView;
    public final List<TrueFalse> mQuestionBank = generateQuestionBank();
    private int mCurrentIndex = 0;
    public final int mNumberOfQuestions = this.mQuestionBank.size();
    public final Random _mRandomizer = new Random(System.currentTimeMillis());
    
    private ArrayList<TrueFalse> generateQuestionBank() {
        ArrayList<TrueFalse> tmp = new ArrayList<TrueFalse>();
        tmp.add(new TrueFalse(R.string.question_africa, false));
        tmp.add(new TrueFalse(R.string.question_mideast, false));
        tmp.add(new TrueFalse(R.string.question_nz, false));
        tmp.add(new TrueFalse(R.string.question_americas, true));
        tmp.add(new TrueFalse(R.string.question_asia, true));
        tmp.add(new TrueFalse(R.string.question_oceans, true));
        return tmp;
    }

    private void getNextQuestion() {
        this.mCurrentIndex = this._mRandomizer.nextInt(this.mNumberOfQuestions);
        int question = this.mQuestionBank.get(mCurrentIndex).getQuestion();
        this.mQuestionTextView.setText(question);
    }
    
    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = this.mQuestionBank.get(this.mCurrentIndex).isTrueQuestion();
        
        int messageResId = 0;
        
        if (userPressedTrue == answerIsTrue) {
            messageResId = R.string.correct_toast;
        } else {
            messageResId = R.string.incorrect_toast;
        }
        
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        
        this.mQuestionTextView = (TextView)findViewById(R.id.question_text_view);
        this.getNextQuestion();

        this.mTrueButton = (Button) findViewById(R.id.true_button);
        this.mTrueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuizActivity.this.checkAnswer(true);
            }
        });
        
        this.mFalseButton = (Button) findViewById(R.id.false_button);
        this.mFalseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuizActivity.this.checkAnswer(false);
            }
        });
        
        this.mNextButton = (Button) findViewById(R.id.next_button);
        this.mNextButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                QuizActivity.this.getNextQuestion();
            }
        });
        
        this.getNextQuestion();        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_quiz, menu);
        return true;
    }
}
