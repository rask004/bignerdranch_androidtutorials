/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.androidTutorial;

/**
 *
 * @author Roland
 */
public class TrueFalse {
    
    private int mQuestion;
    private boolean mTrueQuestion;
    
    public TrueFalse(int question, boolean trueQuestion){
        this.mQuestion = question;
        this.mTrueQuestion = trueQuestion;
    }
    
    public int getQuestion () {
        return this.mQuestion;
    }
    
    public boolean isTrueQuestion () {
        return this.mTrueQuestion;
    }
            
    public void setQuestion(int question) {
        this.mQuestion = question;
    }
            
    public void setTrueQuestion(boolean trueQuestion) {
        this.mTrueQuestion = trueQuestion;
    }

}
