package com.example.bignerdranch.androidtutorials.deprecated;

import java.util.UUID;

import android.support.v4.app.Fragment;

import com.example.bignerdranch.androidtutorials.CrimeFragment;
import com.example.bignerdranch.androidtutorials.SingleFragmentActivity;
import com.example.bignerdranch.androidtutorials.common.Constants;

public class CrimeActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		UUID crimeId = (UUID) getIntent().getSerializableExtra(
				Constants.EXTRA_CRIME_ID);
		return CrimeFragment.newInstance(crimeId);
	}

}
