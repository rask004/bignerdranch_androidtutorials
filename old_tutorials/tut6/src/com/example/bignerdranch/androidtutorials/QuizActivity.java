package com.example.bignerdranch.androidtutorials;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.util.AndroidException;
import android.util.Log;

// ***
public class QuizActivity extends Activity {

    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mNextButton;
    private ImageButton mPreviousButton;
    private Button mCheatButton;
    private TextView mQuestionTextView;
    
    public final List<TrueFalse> mQuestionBank = generateQuestionBank();
    public List<Boolean> mQuestionCheated;
    private int[] mQuestionIndices = {0,0,0,0};
    public final int mNumberOfQuestions = this.mQuestionBank.size();
    public final Random _mRandomizer = new Random(System.currentTimeMillis());
    private boolean mIsCheater = false;
    
    private void generateQuestionCheatedBank(int size) {
    	mQuestionCheated = new ArrayList<Boolean>();
    	
    	for (int i=0; i < size; i++) {
        	mQuestionCheated.add(false);
        }
    }
    
    private ArrayList<TrueFalse> generateQuestionBank() {
        ArrayList<TrueFalse> tmp = new ArrayList<TrueFalse>();
        tmp.add(new TrueFalse(R.string.question_africa, false));
        tmp.add(new TrueFalse(R.string.question_mideast, false));
        tmp.add(new TrueFalse(R.string.question_nz, false));
        tmp.add(new TrueFalse(R.string.question_americas, true));
        tmp.add(new TrueFalse(R.string.question_asia, true));
        tmp.add(new TrueFalse(R.string.question_oceans, true));
        
        generateQuestionCheatedBank(tmp.size());
        
        if (Constants.DEBUG) {
        	Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_CREATEQUESTIONS, new AndroidException());
        }
        return tmp;
    }

    private void getNextQuestion() {
    	int tmp;
    	
    	// record the previous 3 question answer pairs
    	this.mQuestionIndices[0] = this.mQuestionIndices[1];
    	this.mQuestionIndices[1] = this.mQuestionIndices[2];
    	this.mQuestionIndices[2] = this.mQuestionIndices[3];
    	
    	// get a new random question answer pair different from the previous questions
    	tmp = this.mQuestionIndices[3];
    	while (tmp == this.mQuestionIndices[2] || tmp == this.mQuestionIndices[1] || tmp == this.mQuestionIndices[0]) {
    		tmp = this._mRandomizer.nextInt(this.mNumberOfQuestions);
    	}
    	
    	// store the new question answer pair
    	this.mQuestionIndices[3] = tmp;
    	
    	// if this question was previously cheated on, notify the view.
    	mIsCheater = this.mQuestionCheated.get(this.mQuestionIndices[3]);
    	
    	// get the question reference from the new question answer pair 
        int question = this.mQuestionBank.get(this.mQuestionIndices[3]).getQuestion();
        
        // update the UI question text
        this.mQuestionTextView.setText(question);
        
        // update the debug log.
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_NEXTQUESTION);
		}
    }
    
    private void getPreviousQuestion() {
    	
    	//cycle back through previous question pairs.
    	this.mQuestionIndices[3] = this.mQuestionIndices[2];
    	this.mQuestionIndices[2] = this.mQuestionIndices[1];
    	this.mQuestionIndices[1] = this.mQuestionIndices[0];
    	
    	// if this question was previously cheated on, notify the view.
    	mIsCheater = this.mQuestionCheated.get(this.mQuestionIndices[3]);
    	
    	// update the question text in the UI.
        int question = this.mQuestionBank.get(this.mQuestionIndices[3]).getQuestion();
        this.mQuestionTextView.setText(question);
        
        // update the debug log
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_PREVIOUSQUESTION);
		}
    }
    
    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = this.mQuestionBank.get(this.mQuestionIndices[3]).isTrueQuestion();
        
        int messageResId = 0;
        
        if (mIsCheater) {
        	messageResId = R.string.judgement_toast;
        }
        
        else {
	        if (userPressedTrue == answerIsTrue) {
	            messageResId = R.string.correct_toast;
	        } else {
	            messageResId = R.string.incorrect_toast;
	        }
	        
	        if (Constants.DEBUG) {
				Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_CHECKANSWER,
						new AndroidException());
			}
        }
        
		Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Called when the activity is first created.
     */
    @TargetApi(11)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        if (savedInstanceState != null) {
        	this.mQuestionIndices = savedInstanceState.getIntArray(Constants.SAVEDINSTANCEKEY_QUESTIONINDEX);
        	this.mIsCheater = savedInstanceState.getBoolean(Constants.SAVEDINSTANCEKEY_ISCHEATER);
        }
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        	ActionBar actionBar = getActionBar();
        	actionBar.setSubtitle(R.string.app_subtitle);
        }
        
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONCREATE);
			String apiTextString = getString(R.string.api_header) + Integer.toString(Build.VERSION.SDK_INT, 10);
	        TextView apiText = (TextView)findViewById(R.id.api_version);
	        apiText.setText(apiTextString);
		}
        
        this.mQuestionTextView = (TextView)findViewById(R.id.question_text_view);

        this.mTrueButton = (Button) findViewById(R.id.true_button);
        this.mTrueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuizActivity.this.checkAnswer(true);
            }
        });
        
        this.mFalseButton = (Button) findViewById(R.id.false_button);
        this.mFalseButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                QuizActivity.this.checkAnswer(false);
            }
        });
        
        this.mNextButton = (ImageButton) findViewById(R.id.next_button);
        this.mNextButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                QuizActivity.this.getNextQuestion();
            }
        });
        
        this.mPreviousButton = (ImageButton) findViewById(R.id.previous_button);
        this.mPreviousButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
                QuizActivity.this.getPreviousQuestion();
            }
        });
        
        this.mCheatButton = (Button) findViewById(R.id.cheat_button);
        this.mCheatButton.setOnClickListener(new View.OnClickListener() {
            
            @Override
            public void onClick(View v) {
            	
            	Intent cheatIntent = new Intent(QuizActivity.this, CheatActivity.class);
            	
            	// store the answer to the question being cheated on.
            	boolean answerIsTrue = QuizActivity.this.mQuestionBank.get(QuizActivity.this.mQuestionIndices[3]).isTrueQuestion();
            	cheatIntent.putExtra(Constants.EXTRAKEY_ISANSWERTRUE, answerIsTrue);
            	
            	startActivityForResult(cheatIntent, Constants.REQUESTCODE_CHEATACTIVITY);
            }
        });
        
        this.getNextQuestion();   
        
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONSTART);
		}
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONSTOP);
		}
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONRESUME);
		}
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONPAUSE);
		}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_ONDESTROY);
		}
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_SAVESTATE);
		}
    	
		outState.putIntArray(Constants.SAVEDINSTANCEKEY_QUESTIONINDEX, mQuestionIndices);
		
		// convert QuestionCheatedBank to boolean array before saving
		boolean[] tmp = new boolean[mQuestionIndices.length];
		for (int i=0; i < tmp.length; i++) {
			tmp[i] = mQuestionCheated.get(i);
		}
		outState.putBooleanArray(Constants.SAVEDINSTANCEKEY_QUESTIONCHEATEDINDEX,  tmp);
		
		outState.putBoolean(Constants.SAVEDINSTANCEKEY_ISCHEATER, mIsCheater);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	switch (requestCode) {
    		
    		case Constants.REQUESTCODE_CHEATACTIVITY:
    			if (resultCode == Activity.RESULT_OK && data != null) {
    				// user cheated, record this and notify view.
    				mIsCheater = data.getBooleanExtra(Constants.EXTRAKEY_ANSWERSHOWN, false);
    				
    				//get the current question index
    				int cheatedQuestionIndex = this.mQuestionIndices[3];
    				//change the QuestionCheatedBank at this index, to show cheating.
    				mQuestionCheated.set(cheatedQuestionIndex, true);
    						
    				Log.d(Constants.TAG_QUIZACTIVITY, Constants.MSG_USERCHEATED);
    			}
    			break;
    	}
    }
    
}
