package com.example.bignerdranch.androidtutorials;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends Activity {
	
	private boolean mAnswerIsTrue;
	private boolean mAnswerWasShown;
	private TextView mAnswerTextView;
	private Button mShowAnswer;
	
	private void setAnswerShownResult() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra(Constants.EXTRAKEY_ANSWERSHOWN, mAnswerWasShown);
		setResult(Activity.RESULT_OK, resultIntent);
	}

	/**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        
        // initially assume user hasn't cheated.
        // except if rotation occurs and user already cheated.
        if (savedInstanceState != null) {
        	mAnswerWasShown = savedInstanceState.getBoolean(Constants.SAVEDINSTANCEKEY_ANSWERSHOWN);
        } 
        else {
        	mAnswerWasShown = false;
        }
        setAnswerShownResult();
        
        mAnswerIsTrue = getIntent().getBooleanExtra(Constants.EXTRAKEY_ISANSWERTRUE, false);
        mAnswerTextView = (TextView) findViewById(R.id.answerTextView);
        
        mShowAnswer = (Button) findViewById(R.id.showAnswerButton);
        mShowAnswer.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mAnswerIsTrue) {
					mAnswerTextView.setText(R.string.true_button);
				} else {
					mAnswerTextView.setText(R.string.false_button);
				}
				
				// note that the user cheated.
				mAnswerWasShown = true;
				setAnswerShownResult();
			}
		});
        
        if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_ONCREATE);
		}
    }
    
    public void onStart() {
    	super.onStart();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_ONSTART);
		}
    }
    
    public void onStop() {
    	super.onStop();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_ONSTOP);
		}
    }
    
    public void onResume() {
    	super.onResume();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_ONRESUME);
		}
    }
    
    public void onPause() {
    	super.onPause();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_ONPAUSE);
		}
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_ONDESTROY);
		}
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (Constants.DEBUG) {
			Log.d(Constants.TAG_CHEATACTIVITY, Constants.MSG_SAVESTATE);
		}
    	outState.putBoolean(Constants.SAVEDINSTANCEKEY_ANSWERSHOWN, mAnswerWasShown);
    }    
}
