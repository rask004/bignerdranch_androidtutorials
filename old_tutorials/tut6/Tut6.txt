Manifest compatability settings:

Minimum SDK: the minimun API level or OS version that installation is permitted on.
Will not install on OSes with an API level below this.

Target SDK: the preferred API level or OS version.
Features specific to API levels after the target SDK, will not appear in this app.
e.g. if target is API level 16, and an action menu bar is not standardised until level 18, then 
the action menu bar should not appear. 


	Obsolete APIs		| Minimum API								Target API |		Future APIs
	(not installable)	| <--   features used are within this range	       --> |		Features not used.
	
Note: for eclipse projects, these settings are also present in app_compat_vX folders and in project
property files.

 