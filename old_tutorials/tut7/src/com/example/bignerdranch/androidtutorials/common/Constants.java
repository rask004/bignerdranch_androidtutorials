package com.example.bignerdranch.androidtutorials.common;

public class Constants {
	public static final Boolean DEBUG = true;
	
	public static final int REQUESTCODE_OTHERACTIVITY = 0;
	
	public static final String SAVEDINSTANCEKEY = "SAVEDINSTANCE";

	public static final String TAG_CRIMEACTIVITY = "CrimeActivity";

	public static final String MSG_ONCREATE = "onCreate called.";
	public static final String MSG_ONRESUME = "onResume called.";
	public static final String MSG_ONPAUSE = "onPause called.";
	public static final String MSG_ONSTOP = "onStop called.";
	public static final String MSG_ONDESTROY = "onDestroy called.";
	public static final String MSG_ONSTART = "onStart called.";
	public static final String MSG_SAVESTATE = "onSaveInstanceState called.";
	public static final String MSG_OTHERACTIVITY_RESULT = "Activity Result received with intent.";
}
