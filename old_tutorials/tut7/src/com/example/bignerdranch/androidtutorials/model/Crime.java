package com.example.bignerdranch.androidtutorials.model;

import java.util.UUID;

public class Crime {
	
	private String mTitle;
	private UUID mId;
	
	public Crime() {
		// create unique id
		mId = UUID.randomUUID();
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	public UUID getID() {
		return mId;
	}

}
