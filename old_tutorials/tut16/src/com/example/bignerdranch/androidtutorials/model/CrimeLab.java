package com.example.bignerdranch.androidtutorials.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;

public class CrimeLab {

	private static CrimeLab sCrimeLab;
	private Context mAppContext;
	private List<Crime> mCrimes;

	private CrimeLab(Context appContext) {
		mAppContext = appContext;
		mCrimes = new ArrayList<Crime>();

	}

	public void addCrime(Crime c) {

		mCrimes.add(c);
	}

	public ArrayList<Crime> getCrimes() {
		return (ArrayList<Crime>) mCrimes;
	}

	public Crime getCrime(UUID id) {
		for (Crime c : mCrimes) {
			if (c.getID().equals(id)) {
				return c;
			}
		}
		return null;

	}

	public static CrimeLab get(Context c) {
		if (sCrimeLab == null) {
			sCrimeLab = new CrimeLab(c.getApplicationContext());
		}
		return sCrimeLab;
	}

}
