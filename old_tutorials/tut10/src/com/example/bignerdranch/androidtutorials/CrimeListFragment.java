package com.example.bignerdranch.androidtutorials;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bignerdranch.androidtutorials.common.Constants;
import com.example.bignerdranch.androidtutorials.model.Crime;
import com.example.bignerdranch.androidtutorials.model.CrimeLab;

public class CrimeListFragment extends ListFragment {

	private List<Crime> mCrimes;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().setTitle(R.string.crimes_title);
		mCrimes = CrimeLab.get(getActivity()).getCrimes();

		CrimeAdapter adapter = new CrimeAdapter(mCrimes);
		setListAdapter(adapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Crime c = ((CrimeAdapter) getListAdapter()).getItem(position);
		Log.d(Constants.TAG_CRIMELISTFRAGMENT, c.getTitle() + "was clicked");

		// Start a new CrimeActivity instance.
		Intent crimeActivityIntent = new Intent(getActivity(),
				CrimeActivity.class);
		crimeActivityIntent.putExtra(Constants.EXTRA_CRIME_ID, c.getID());
		startActivity(crimeActivityIntent);
	}

	private class CrimeAdapter extends ArrayAdapter<Crime> {
		public CrimeAdapter(List<Crime> crimes) {
			super(getActivity(), 0, crimes);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// inflate a view if none given
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.list_item_crime, null);
			}

			// configure view for the related crime
			Crime c = getItem(position);

			TextView titleTextView = (TextView) convertView
					.findViewById(R.id.crime_list_item_titleTextView);
			titleTextView.setText(c.getTitle());
			TextView dateTextView = (TextView) convertView
					.findViewById(R.id.crime_list_item_dateTextView);
			dateTextView.setText(c.getDate().toString());
			CheckBox solvedCheckBox = (CheckBox) convertView
					.findViewById(R.id.crime_list_item_solvedCheckBox);
			solvedCheckBox.setChecked(c.isSolved());

			return convertView;

		}
	}

	@Override
	public void onResume() {
		super.onResume();

		((CrimeAdapter) getListAdapter()).notifyDataSetChanged();
	}


}
