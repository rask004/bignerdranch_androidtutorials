package com.example.bignerdranch.androidtutorials;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

import com.example.bignerdranch.androidtutorials.common.Constants;

public class DatePickerFragment extends DialogFragment {

	private Date mDate;

	private void sendResult(int resultCode) {
		if (getTargetFragment() == null) {
			return;
		}

		Intent datePickerResultIntent = new Intent();
		datePickerResultIntent.putExtra(Constants.EXTRA_DATE, mDate);

		getTargetFragment().onActivityResult(getTargetRequestCode(),
				resultCode, datePickerResultIntent);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// store the supplied date and update the calendar state.
		mDate = (Date) getArguments().getSerializable(Constants.EXTRA_DATE);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mDate);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		View v = getActivity().getLayoutInflater().inflate(
				R.layout.dialog_date, null);

		DatePicker datePicker = (DatePicker) v
				.findViewById(R.id.dialog_date_datePicker);
		datePicker.init(year, month, day, new OnDateChangedListener() {

			@Override
			public void onDateChanged(DatePicker view, int year, int month,
					int day) {
				// update the stored date to the new Date.
				mDate = new GregorianCalendar(year, month, day).getTime();

				// rebundle the new date for the fragment view to update
				// keep it updated across rotation.
				getArguments().putSerializable(Constants.EXTRA_DATE, mDate);

			}

		});

		return new AlertDialog.Builder(getActivity())
				.setView(v)
				.setTitle(R.string.date_picker_title)
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								sendResult(Activity.RESULT_OK);

							}

						}).create();

	}

	public static DatePickerFragment newInstance(Date d) {
		DatePickerFragment datePicker = new DatePickerFragment();
		Bundle args = new Bundle();
		args.putSerializable(Constants.EXTRA_DATE, d);

		datePicker.setArguments(args);

		return datePicker;

	}

}
