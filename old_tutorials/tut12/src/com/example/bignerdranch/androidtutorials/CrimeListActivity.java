package com.example.bignerdranch.androidtutorials;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

public class CrimeListActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new CrimeListFragment();
	}
	
	@TargetApi(11)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// compatability check for action bars.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Log.d("Compatability Check ", "" + Build.VERSION.SDK_INT + " >= "
					+ Build.VERSION_CODES.HONEYCOMB);

			ActionBar actionBar = getActionBar();
			Log.d("Action Bar state ", "" + actionBar);
		}

	}
	
	
	
	

}
