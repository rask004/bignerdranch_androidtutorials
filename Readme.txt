A series of tutorials based on the BigNerdRanch - Android Programming guide.

Each branch represents a tutorial section, in progress or completed. Each branch has a tut*.txt file with shorttype explanations of the tutorial section and activities. 

For each branch, previous tutorial material is moved under the old_tutorials folder for backward referral. 

Previous experience with Java is assumed when browsing this material.

The Author does not guarantee that all code or other material will be fully self-explanatory. It is advised to utilise the API guide at http://developer.android.com/guide/index.html  to enhance understanding as necessary.

The guide book can be found at https://www.bignerdranch.com/we-write/android-programming/ .