package com.example.bignerdranch.androidtutorials.model;

import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.bignerdranch.androidtutorials.common.Constants;

public class Crime {
	
	private String mTitle;
	private UUID mId;
	private Date mDate;
	private boolean mSolved;
	
	public Crime(JSONObject json) throws JSONException {
		mId = UUID.fromString(json.getString(Constants.JSONKEY_ID));
		if (json.has(Constants.JSONKEY_TITLE)) {
			mTitle = json.getString(Constants.JSONKEY_TITLE);
		} else {
			mTitle = "";
		}
		mDate = new Date(json.getLong(Constants.JSONKEY_DATE));
		mSolved = json.getBoolean(Constants.JSONKEY_SOLVED);
	}

	public Crime() {
		// create unique id
		mId = UUID.randomUUID();
		mDate = new Date();
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject json = new JSONObject();

		json.put(Constants.JSONKEY_ID, mId);
		json.put(Constants.JSONKEY_TITLE, mTitle);
		json.put(Constants.JSONKEY_DATE, mDate);
		json.put(Constants.JSONKEY_SOLVED, mSolved);

		return json;
	}

	@Override
	public String toString() {
		String crimeString = "";
		crimeString = crimeString + Constants.JSONKEY_TITLE + "=" + mTitle
				+ ",";
		crimeString = crimeString + Constants.JSONKEY_ID + "=" + mId + ",";
		crimeString = crimeString + Constants.JSONKEY_DATE + "="
				+ mDate.toString() + ",";
		crimeString = crimeString + Constants.JSONKEY_SOLVED + "=" + mSolved;
		return crimeString;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	public UUID getID() {
		return mId;
	}

	public Date getDate() {
		return this.mDate;
	}

	public void setDate(Date mDate) {
		this.mDate = mDate;
	}

	public boolean isSolved() {
		return this.mSolved;
	}

	public void setSolved(boolean mSolved) {
		this.mSolved = mSolved;
	}

}
