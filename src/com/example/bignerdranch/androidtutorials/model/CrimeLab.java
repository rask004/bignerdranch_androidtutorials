package com.example.bignerdranch.androidtutorials.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

import com.example.bignerdranch.androidtutorials.CriminalIntentJSONSerializer;
import com.example.bignerdranch.androidtutorials.common.Constants;

public class CrimeLab {

	private static CrimeLab sCrimeLab;
	private Context mAppContext;
	private List<Crime> mCrimes;
	private CriminalIntentJSONSerializer mSerializer;

	private CrimeLab(Context appContext) {
		mAppContext = appContext;
		mSerializer = new CriminalIntentJSONSerializer(mAppContext,
				Constants.JSON_FILENAME_CRIMELAB);
		try {
			mCrimes = mSerializer.loadArray();
		} catch (Exception e) {
			mCrimes = new ArrayList<Crime>();
			Log.e(Constants.JSON_CRIMELAB_TAG, "Error loading crimes: ", e);
		}
	}

	public void addCrime(Crime c) {

		mCrimes.add(c);
	}

	public boolean saveCrimes() {
		try {
			mSerializer.saveArray((ArrayList<Crime>) mCrimes);
			if (Constants.DEBUG) {
				Log.d(Constants.JSON_CRIMELAB_TAG, "crimes saved to file.");
			}
			return true;
		} catch (Exception e) {
			Log.e(Constants.JSON_CRIMELAB_TAG, "Error saving crimes: ", e);
			return false;
		}
	}

	public ArrayList<Crime> getCrimes() {
		return (ArrayList<Crime>) mCrimes;
	}

	public Crime getCrime(UUID id) {
		for (Crime c : mCrimes) {
			if (c.getID().equals(id)) {
				return c;
			}
		}
		return null;

	}

	public static CrimeLab get(Context c) {
		if (sCrimeLab == null) {
			sCrimeLab = new CrimeLab(c.getApplicationContext());
		}
		return sCrimeLab;
	}

}
