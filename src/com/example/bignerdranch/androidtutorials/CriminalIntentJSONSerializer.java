package com.example.bignerdranch.androidtutorials;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.example.bignerdranch.androidtutorials.common.BaseJSONSerializer;
import com.example.bignerdranch.androidtutorials.model.Crime;

public class CriminalIntentJSONSerializer extends BaseJSONSerializer {

	public CriminalIntentJSONSerializer(Context c, String f) {
		super(c, f);
	}

	public ArrayList<Crime> loadArray() throws JSONException, IOException {

		List<JSONObject> jArray = super.p_loadJSONList();
		ArrayList<Crime> crimes = new ArrayList<Crime>();

		for (JSONObject j : jArray) {
			crimes.add(new Crime(j));
		}

		return crimes;

	}

	public void saveArray(ArrayList<Crime> crimes) throws JSONException,
			IOException {
		super.p_saveToJSONFile(crimes);

	}
}
