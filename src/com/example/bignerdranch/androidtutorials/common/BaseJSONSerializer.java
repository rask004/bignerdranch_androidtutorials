package com.example.bignerdranch.androidtutorials.common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;

public abstract class BaseJSONSerializer {

	private Context mContext;
	private String mFilename;

	public BaseJSONSerializer(Context c, String f) {
		mContext = c;
		mFilename = f;
	}

	protected <E> void p_saveToJSONFile(ArrayList<E> savingArray)
			throws JSONException,
			IOException {

		// first check if all elements in savingArray are JSONable.
		// if all are, use toJSON method on each with a JSONArray.
		// otherwise use toString on each with some conversion, and write JSON
		// manually?
		// or simply write to strings.

		JSONArray jArray = new JSONArray();

		String errMsg = "Object in ArrayList param does not have toJSON method, and could not be converted to JSONObject.";
		errMsg = errMsg
				+ "\nadd a toJSON method, or change toString to use \"key1:value1,key2:value2,...\" format.";

		for (E o : savingArray) {
			try {
				Class<?> args[] = null;
				Method m = o.getClass().getMethod("toJSON", args);
				jArray.put(m.invoke(o, (Object[]) args));
			} catch (NoSuchMethodException | InvocationTargetException
					| IllegalAccessException | IllegalArgumentException e) {

				// assuming the toString method can be transformed to a JSON
				// format, i.e. key1:value1, key2:value2
				String oStr[] = o.toString().split(",");
				if (oStr.length == 0) {
					throw new JSONException(errMsg);
				}

				JSONObject j = new JSONObject();

				// resolves gotcha of training comma or multiple commas.
				for (String s : oStr) {
					if (s.length() == 0) {
						continue;
					}
					String kv[] = s.split("=");
					if (kv.length != 2) {
						throw new JSONException(errMsg);
					}

					j.put(kv[0], kv[1]);
				}
				jArray.put(j);

			}
		}

		Writer writer = null;
		try {
			OutputStream out = mContext.openFileOutput(mFilename,
					Context.MODE_PRIVATE);
			writer = new OutputStreamWriter(out);
			writer.write(jArray.toString());
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	protected List<JSONObject> p_loadJSONList() throws JSONException,
			IOException {

		List<JSONObject> jArray = new ArrayList<JSONObject>();

		// read json data into stringbuilder
		BufferedReader reader = null;
		try {
			InputStream in = mContext.openFileInput(mFilename);
			reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder jsonString = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				jsonString.append(line);
			}

			// convert into jsonarray
			JSONArray loadingArray = (JSONArray) new JSONTokener(
					jsonString.toString()).nextValue();
			for (int i = 0; i < loadingArray.length(); i++) {
				jArray.add(loadingArray.getJSONObject(i));
			}

		} catch (FileNotFoundException fnf) {
			// First happens when starting fresh. Should ignore.
		} finally {
			if (reader != null) {
				reader.close();
			}
		}

		return jArray;
	}

}
