package com.example.bignerdranch.androidtutorials.common;

public class Constants {
	public static final Boolean DEBUG = true;
	
	public static final String JSON_CRIMELAB_TAG = "CrimeLab";
	public static final String JSON_FILENAME_CRIMELAB = "crimes.json";

	public static final String JSONKEY_TITLE = "title";
	public static final String JSONKEY_ID = "id";
	public static final String JSONKEY_DATE = "date";
	public static final String JSONKEY_SOLVED = "solved";

	public static final String JSONVALUE_SOLVED_TRUE = "true";
	public static final String JSONVALUE_SOLVED_FALSE = "false";

	public static final int REQUESTCODE_DATE = 0;
	
	public static final String SAVEDINSTANCEKEY = "SAVEDINSTANCE";

	public static final String DIALOG_DATE = "date";

	public static final String EXTRA_CRIME_ID = "EXTRA_Crime_UUID";
	public static final String EXTRA_DATE = "EXTRA_DatePicker_DATE";

	public static final String TAG_CRIMEACTIVITY = "CrimeActivity";
	public static final String TAG_SINGLEFRAGMENT = "SingleFragmentActivity";
	public static final String TAG_CRIMELISTFRAGMENT = "CrimeListFragment";

	public static final String MSG_COMPATABILITY_ACTIONBAR_GET = "getActionBar called. ";
	public static final String MSG_COMPATABILITY_ACTIONBAR_ISNULL = "getActionBar state is null. ";
	public static final String MSG_ONCREATE = "onCreate called.";
	public static final String MSG_ONRESUME = "onResume called.";
	public static final String MSG_ONPAUSE = "onPause called.";
	public static final String MSG_ONSTOP = "onStop called.";
	public static final String MSG_ONDESTROY = "onDestroy called.";
	public static final String MSG_ONSTART = "onStart called.";
	public static final String MSG_SAVESTATE = "onSaveInstanceState called.";
	public static final String MSG_OTHERACTIVITY_RESULT = "Activity Result received with intent.";
}
